pragma solidity >=0.4.22 <0.7.0;

contract CrowdFunding {
    address payable public beneficiary;    
        
    uint public total_fund;
    uint public target;
    uint public EndTime;
    
    mapping(address => uint) senders;

    event Funding(address sender, uint amount);
    event FundingEnded(uint amount);
    bool ended;
        

    constructor(
        uint _target,
        uint _biddingTime,
        address payable _beneficiary
    ) public {
        beneficiary = _beneficiary;
        total_fund  = 0;
        target = _target;
        EndTime = now + _biddingTime;
        ended = false;
    }

    function fund() public payable {

        require(
            now <= EndTime,
            "Funding already ended."
        );

        total_fund = total_fund + msg.value;
        senders[msg.sender] = msg.value;
        emit Funding(msg.sender, msg.value);
    }

    /// Withdraw a bid that was overbid.
    function withdraw() public returns (bool) {
        uint amount = senders[msg.sender];
        if ((amount > 0) && (ended == true) && (total_fund < target)) {

            senders[msg.sender] = 0;

            if (!msg.sender.send(amount)) {                
                senders[msg.sender] = amount;
                return false;
            }
        }
        return true;
    }

    function fundingEnd() public {

        // 1. Conditions
        require(now >= EndTime, "Funding not yet ended.");
        require(!ended, "fundingEnd has already been called.");        

        // 2. Effects
        ended = true;
        emit FundingEnded(total_fund);
        // 3. Interaction
        if (total_fund >= target) {
            beneficiary.transfer(total_fund);
        }
    }
}