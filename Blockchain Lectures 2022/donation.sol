pragma solidity >=0.4.22 <0.7.0;

contract Donation {

    address payable public beneficiary;    

    constructor(        
        address payable _beneficiary
    ) public {
        beneficiary = _beneficiary;        
    }

    function donate() public payable {        
        beneficiary.transfer(msg.value);
    }
}