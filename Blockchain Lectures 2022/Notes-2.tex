\documentclass[12pt]{article}
\usepackage{amsfonts,amsthm,amsmath,amssymb}
\usepackage{array}
\usepackage{epsfig}
\usepackage{fullpage}
\usepackage{color}
\usepackage[colorlinks=true,citecolor=blue]{hyperref}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{tikz}
\usepackage{subfig}

%%%%%%%%%%%%%% VINOD's DEFS %%%%%%%%%%%%%%%%%%%%

\newcommand{\eqdef}{\stackrel{\small \sf def}{=}}
\newcommand{\lat}{\mathcal{L}}
\newcommand{\ppd}{\mathcal{P}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\def\MT{Merkle Tree }

\newcommand{\vecbt}{\tilde{\vecb}}

% Inner product
%\newcommand{\inner}[1]{\langle {#1} \rangle}

% Red text
%\newcommand{\red}[1]{\textcolor{red}{#1}}



%-------------------------------DO NOT modify below-------------
\begin{document}
\input{preamble.tex}

\lecture{\#2}{ }{Hongsheng Zhou}{Shuang Hu}

\section{Merkle Tree}
\label{sec:one}
\paragraph{Intuition}
In cryptography and computer science, \MT is a binary hash tree in which every leaf node is labelled with the hash of a data block, and every non-leaf node is labelled with the cryptographic hash of the labels of its child nodes. This design of hash trees allow efficient and secure verification of the contents of large data structures. Demonstrating that a leaf node is a part of a given binary hash tree requires computing a number of hashes proportional to the logarithm of the number of leaf nodes of the tree.

\subsection{\MT In Typical Scenarios} \label{sec:mtapp}
Data storage and retrieval are typical scenarios where \MT is needed. Consider a client who uploads a file $x$ to a server. When the client later retrieves $x$, it wants to make sure that the server returns the original, unmodified file. The client can locally store the short digest $h := H(x)$ instead of the full content of $x$; when the server returns a candidate file $x'$ the client need only check $H(x')\overset{?}{=} h$, if $H(x') = h$ then he knows the file has not been changed.

Furthermore, if the client wants to store multiple files $x_1\cdots x_t$ to the server while can also verify some files retrieved, instead of saving the hash of the concatenation of all the $t$ files or the independent hashes of all the $t$ files, he can construct a \MT over the input files in following steps to save the storage and verification cost.
\begin{enumerate}
\item Calculate the hash of every file in $x_1\cdots x_t$ and place these hashes at the leaves of a binary tree on the level, which we called \emph{layer 1}( Assume that $t$ is the logarithm of $2$, if not then add some strings to make it be log of $2$, or just use an incomplete binary tree, depending on the application).
\item Let every two of the leaves be the child nodes of a node which is on one layer above, i.e. \emph{layer 2}, and the node's value is the hash value of the concatenation of its two child nodes; similarly, every two nodes at \emph{layer 2} are the child nodes of a node at \emph{layer 3} whose value is also the hash value of the concatenation of its two child nodes, and so forth; until there is only one node, whose value is called the Merkle Tree \emph{root}, denoted as $\mathcal{MT}$.% at some layer, to be exact, \emph{layer $\ceil{\log t}$}, which is the top layer. 
\item The client stores the Merkle Tree\emph{root}, which is the same size as one single hash output. When the client want to verify a file, say $x_3$, first the server only needs to send him the full content of $x_3$ and the hashes of all other files among $x_1,\cdots x_t$, based on these data he can then construct a Merkle Tree and get the root $\mathcal{MT}'$ himself and check if  $\mathcal{MT}'=\mathcal{MT}$, if so then he knows that $x_3$ is unmodified. 

As we can see, using \MT can reduce the client's storage cost as well as verification cost for large data storage and verification.
\end{enumerate}

Merkle trees are used by both Bitcoin and Ethereum. And hashing is usually conducted using the SHA-2 cryptographic hash function.

\subsection{Properties of \MT}
The following properties of \MT make it a desirable tool for verification of large data sets.

\begin{itemize}
\item \textbf{Small Storage.} Merkle trees are built on Hash functions, and the size of a $\mathsf{Merkle Root}$ is the same with the size of a hash, while a $\mathsf{Merkle Root}$ offers a digest for much more contents than a regular hash. Therefore using the structure of \MT is an efficient to store digest of large data sets.
\item \textbf{Efficient Verification}. As the Transaction Verification process described above, the number of hashes required for verifying a transaction is in logarithm with the total number of transactions. Therefore the verification is very efficient since computing a small number of hashes is really fast.
\item \textbf{Collision Resistance.} If the input size of a \MT is fixed and the hash function used in the \MT is collision resistant, then the $\mathsf{Merkle Root}$, which can be considered the output of a \MT function, is also collision resistant. That a function is collision resistant indicates that no two different inputs of the function will result in the same output. Considering the scenario of blockchain for example.The practical meaning of $\mathsf{Merkle Root}$'s \emph{collision resistance} property is that, once a block along with its transaction set is published, the \MT for the transactions set is fixed and public, in any later time if an adversary want to modify any transaction in the block, the new $\mathsf{Merkle Root}$ of the modified transaction set will not be the same as the original $\mathsf{Merkle Root}$, which is public to participants in the system, thus adversarial transaction modification will be detected.
\end{itemize}

Next section we introduce a simple example of the \MT, see Figure ~\ref{fig:MTbc}. 



\section{\MT in Blockchain}\label{sec:mtbc}
 In Blockchain technology, \MT plays a fundamental part since it allows for efficient and secure verification of transactions. Specifically, in Bitcoin and similar systems, \MT summarizes all the transactions in a block by producing a \emph{digital fingerprint (or digest)} of the entire set of transactions, thereby enabling a user to verify whether or not a transaction is included in a block and unmodified. 
 
 A \MT of Bitcoin transactions is constructed according to steps in the previous section as in Figure~\ref{fig:MTbc}, where we consider a simple example of only four transactions $\mathsf{A, B, C, D}$ in a block.
\begin{figure}[htbp]
\begin{center}
\includegraphics[scale=0.5]{MTbc.png}
\caption{A Merkle Tree of blockchain transactions}
\label{fig:MTbc}
\end{center}
\end{figure}
 
 \paragraph{Transaction Verification.} When anyone want to verify if transaction $\mathsf{B}$ is legally included in the transaction set in Figure~\ref{fig:MTbc}, he can find $\mathsf{Hash A, Hash CD, Merkle Root}$ of the block from the blockchain and compute 
 $$\mathsf{Hash AB'=H(Hash A \parallel Hash B)}$$  
 $$\mathsf{Merkle Root'= H(Hash AB' \parallel Hash CD)}$$ 
  where $\parallel$ means concatenation, and then he can check whether $\mathsf{Merkle Root'}$ equals $\mathsf{Merkle Root}$, if the equivalence holds then he is convinced that transaction $\mathsf{B}$ is a legal transaction in this block.




\end{document}
