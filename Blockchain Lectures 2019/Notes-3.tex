\documentclass[12pt]{article}
\usepackage{amsfonts,amsthm,amsmath,amssymb}
\usepackage{array}
\usepackage{epsfig}
\usepackage{fullpage}
\usepackage{color}
\usepackage[colorlinks=true,citecolor=blue]{hyperref}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{tikz}
\usepackage{subfig}

%%%%%%%%%%%%%% VINOD's DEFS %%%%%%%%%%%%%%%%%%%%

\newcommand{\eqdef}{\stackrel{\small \sf def}{=}}

\def\HF{hash function }

\newcommand{\vecbt}{\tilde{\vecb}}

% Inner product
\newcommand{\inner}[1]{\langle {#1} \rangle}

% Red text
\newcommand{\red}[1]{\textcolor{red}{#1}}



%-------------------------------DO NOT modify below-------------
\begin{document}
\input{preamble.tex}

\lecture{\#3}{}{Hongsheng Zhou}{Shuang Hu}

%%%% body goes in here %%%%

\section{Bitcoin Mining}
Bitcoin is a decentralized digital currency, and at the core of it is the \emph{consensus mining} process, also called Proof of Work (PoW). Bitcoin mining is about solving a PoW puzzle, which we simplify as in Definition~\ref{def:pow}.

\begin{definition}[PoW puzzle]\label{def:pow}
A simplified PoW puzzle is a Hash equation on block header as follows,:

\begin{equation}\label{eq:mine}
 H(\mathsf{Prevhash}, \mathsf{Context}, \mathsf{Solution})\le 2^{160-T}
 \end{equation}
 where $(\mathsf{Prevhash}, \mathsf{Context}, \mathsf{Solution})$ make up the block header, and a complete block is comprised of the block header and the transactions. See Figure ~\ref{fig:blockstruc} for a clear view of Bitcoin block structure.

$H$ is a moderately hard cryptographic hash function that takes input data of any size and outputs data of a fixed size (in Bitcoin, the output is 160 bits), and the output has no structure such that given an input one cannot predict the output. $\mathsf{Prevhash}$ is the hash of the previous block. $\mathsf{Context}$ refers to the Merkle Root of the transactions and the timestamp. $\mathsf{Solution}$ mainly include an arbitrary nonce number in $\{0,1\}^{32}$.   $T$ is the difficulty parameter that specifies a target range of the hash value, specifically, a hash value starting with $T$ bits of zero.

\end{definition}

\begin{figure}[htbp]
\begin{center}
\includegraphics[scale=0.2]{blockstructure.jpg}
\caption{Block Structure}
\label{fig:blockstruc}
\end{center}
\end{figure}

Basically, miners need to compute the hashes of many different block headers to get a valid solution for the puzzle, finding a solution is equal to finding a valid block that extends the blockchain; $\mathsf{Prevhash}$ is fixed and public, miners can only modify the  $\mathsf{Context}$ and $\mathsf{Solution}$ fields. For simplicity, miners can determine the $\mathsf{Context}$ first and fix it, then modify only the $\mathsf{Solution}$ field to try to get the solution.

From Equation ~\ref{eq:mine}, we can calculate that the probability of an arbitrary hash output being a valid solution is $p=\frac{1}{2^T}$. For every PoW puzzle, the first miner to find a valid solution will publish the block and get the block reward, while others will get nothing. Therefore if $T$ is large and there are many miners competing for solving the puzzle, the probability of getting the solution is very small for small individual miners, and the computing power they invested may be wasted with no return. Mining pools are thus created to balance the risks of small miners.

\section{Mining Pool Design}
A mining pool is a joint effort among several miners to work on finding blocks together, and split the rewards among the participants in proportion to their contribution. The expected revenue from pooled mining is close to that of solo mining, but the risk is significantly smaller; at the same time, the pool operator will collect fees for the pool management, thus mining pools seem like a win-win method.

%skip
\medskip \noindent
We introduce one type of mining pool design, which enables the miners in the pool (i.e., the pool members) to find \emph{shares} as valid partial solution, which are then collected and verified by the pool operator. With certain probability a true \emph{solution} can be found among the many  \emph{shares}.

\paragraph{\emph{Shares}.}  Assuming at some point the valid PoW solutions are specified as hash outputs starting with $T_1$ zeros, the pool operator can reduce the difficulty to hash outputs starting with $T_2$ zeros ($T_2 \le T_1$),  which are called \emph{shares}. In this case, every partial solution has $\frac{1}{2^{T_1-T_2}}$ probability of being a true \emph{solution}. Note that $T_2$ should be a moderate value that matches the computing power of the pool members and the pool's overall profit.  

Mining pools should be designed smartly for preventing cheating by miners as well as verifying miners' contribution. And every pool member's working capacity could be measured by the \emph{number of valid shares} he submitted, approximately proportionally to his \emph{computing power}. 

%skip
\medskip \noindent
Next we the describe the design of a mining pool, as shown in Figure ~\ref{fig:pooldesign}, assuming the current difficulty parameter is $T_1$:

\begin{figure}[htbp]
\begin{center}
\includegraphics[scale=0.5]{pooldesign.png}
\caption{Ming Pool Design}
\label{fig:pooldesign}
\end{center}
\end{figure}

\begin{enumerate}
\item A pool operator $\mathcal{P}$ first chooses the transactions to be included in the block, including a Coinbase transaction sending the reward to $\mathcal{P}$'s address, thus $\mathsf{Context}$ is determined; and $\mathcal{P}$ redefines a share difficulty parameter $T_2$ ($T_2 \le T_1$) and sends it together with $\mathsf{Context}$ to the pool members.

\item With the public $\mathsf{Prevhash}$, $\mathcal{P}$ redefines the current puzzle for the pool members as
$$H(\mathsf{Prevhash}, \mathsf{Context}, \mathsf{Solution})\le2^{160-T_2}$$
and assigns it to them.

\item The pool members search in the $\mathsf{Solution}$ space to find \emph{shares}, once found, submit them to $\mathcal{P}$.

\item $\mathcal{P}$ verifies the \emph{shares} by the rules pre-specified; and if a true \emph{solution} is found among them, he publishes it and get the block reward. Except for the manage fees, the reward is distributed proportionally to the pool members according to the \emph{number of valid shares} they submitted.
\end{enumerate}


 
\section{Selfish Mining Problem}

 In some aspects, Bitcoin mining pools are good due to the effect of balancing the risks of individual miners, but they also bring some serious problems for Bitcoin system, as Eyal et al. mentioned in the paper \emph{Majority is not Enough: Bitcoin Mining is Vulnerable}. 

%skip
\medskip \noindent
 In the rational setting, assuming all the miners want to earn more money in mining, a fair blockchain protocol should be incentive-compatible and miners can get reward proportionally to their computing power, even for arbitrarily malicious miners. While Bitcoin protocol is not incentive-compatible even in the presence of an honest majority. Malicious mining pools can get more profit than the fair reward through \emph{selfish mining} strategies. 
 
Eyal et al. described a concrete  \emph{selfish mining} strategy, simplified as follows:  a mining pool $\mathcal{P}$ finds more blocks (eg. 3 more blocks) than others within certain period, instead of publishing them immediately, $\mathcal{P}$ withholds the 3 blocks; then if another miner $\mathcal{A}$ publishes 1 block, $\mathcal{P}$ publishes 1 block (now $\mathcal{P}$ is 2 blocks ahead of others); after certain time if another miner $\mathcal{B}$ publishes 1 block, $\mathcal{P}$ immediately publishes the rest 2 blocks. Due to the rule that ``the longest chain is the valid chain'' in Bitcoin, the chain consisting of $\mathcal{P}$'s blocks is valid and he gets the rewards, but the blocks of $\mathcal{A}$ and $\mathcal{B}$ will be invalid and their computing power in the blocks are wasted. 
 
Eyal et al.'s analysis shows that once a pool's computing power exceeds the threshold in certain condition, the pool can increase its fair revenue by running the \emph{selfish mining} strategy; and over 2/3 of the participants need to be honest to protect against selfish mining, under the most optimistic of assumptions.




\end{document}
